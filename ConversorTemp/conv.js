
var btnConverter = document.getElementById("btnConverter")

btnConverter.onclick = function () {
    let tempEntrada = Number(document.getElementById("idTemp").value)
    let unidadeEntrada = document.querySelector("idUnidadeOrigem").value
    let unidadeConvesao = document.querySelector("#idUnidadeConvertido").value

    //Conversao da unidade de entrada para Celsius 
    let tempCesius

    switch (unidadeEntrada) {
        case "C":
            tempCesius = tempEntrada
            break;

        case "F":
            tempCesius = (tempEntrada * 5 - 160) / 9
            

        case "K":
            tempCesius = tempEntrada - 273
            break;

        default:
            break;
    }

    //Conversao de celsius para a unidade de saida 
    let tempConvertido
    switch (unidadeConvesao) {
        case "C":
            tempConvertido = tempCesius
            break;
        case "F":
            tempConvertido = (9 * tempCesius + 160) / 5
            break;
        case "K":
            tempConvertido = tempCesius + 273
            break;
        default:
            break;
    }

    document.getElementById("idTempConvertido").value = tempConvertido

    
    // DEBUG:
    //--> Faltou "#" na idUnidadeOrigem para indicar que é um id ao utilizar o querySelector.
    //--> O script foi chamado no head, mas deveria ser chamado no body.
    //--> No primeiro conditional case, em "F", falta um break.
    //--> "Converter para" foi escrito fora de uma tag, como <p>
    //--> A tag button não tinha type.

}
